#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <sys/user.h>
#include <sys/wait.h>

#include <linux/audit.h>
#include <linux/filter.h>
#include <linux/seccomp.h>

#include <fcntl.h>
#include <seccomp.h>
#include <time.h>
#include <unistd.h>

#include <atomic>
#include <cerrno>
#include <csignal>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <set>
#include <string>
#include <string_view>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "toml.hpp"

#define die(...)                                                                                                       \
    do {                                                                                                               \
        fprintf(stderr, "[E:%s:%d] ", __FILE__, __LINE__);                                                             \
        fprintf(stderr, __VA_ARGS__);                                                                                  \
        fprintf(stderr, "\n");                                                                                         \
        exit(1);                                                                                                       \
    } while (0)

bool should_die = 0;
pid_t killed_signal;

struct PidStatus {
    enum State { kBeforeSysCall, kInSysCall };

    pid_t pid;
    State current_state;
    uint64_t lastsysno = 0;

    explicit PidStatus(pid_t pid_) : pid{pid_}, current_state{kBeforeSysCall} {
        ptrace(PTRACE_SETOPTIONS, pid, 0,
               PTRACE_O_TRACESYSGOOD | PTRACE_O_TRACESECCOMP | PTRACE_O_TRACEFORK | PTRACE_O_TRACECLONE |
                   PTRACE_O_EXITKILL | PTRACE_O_TRACEVFORK | PTRACE_O_TRACEEXEC);
    }

    std::optional<user_regs_struct> HandleStatus(int status) {
        if (current_state == kBeforeSysCall) {
            if ((status >> 8) == (SIGTRAP | (PTRACE_EVENT_SECCOMP << 8))) {
                user_regs_struct regs{};
                ptrace(PTRACE_GETREGS, pid, 0, &regs);
                current_state = kInSysCall;
                lastsysno = regs.orig_rax;
                return regs;
            }
        } else {
            if (WIFSTOPPED(status) && WSTOPSIG(status) & (1u << 7)) {
                user_regs_struct regs{};
                ptrace(PTRACE_GETREGS, pid, 0, &regs);
                current_state = kBeforeSysCall;
                return regs;
            }
        }
        return std::nullopt;
    }

    void Resume() {
        if (current_state == kInSysCall) {
            // await syscall exit
            ptrace(PTRACE_SYSCALL, pid, 0, 0);
        } else {
            // await next signal/seccomp event
            ptrace(PTRACE_CONT, pid, 0, 0);
        }
    }

    void SetRegisters(const user_regs_struct &state) {
        ptrace(PTRACE_SETREGS, pid, 0, &state);
    }
};

struct Config {
    int forks_limit;
    std::unordered_set<std::string> read_banned;
    std::unordered_set<std::string> write_allowed;
    std::unordered_set<int> traced_syscalls;
};

const std::string cur_time() {
    time_t now = time(0);
    tm tstruct;
    char buf[9];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);
    return buf;
}
#define LOG(pid, event) log << "[" << cur_time() << "]\t" << pid << '\t' << event << std::endl

const std::filesystem::path curdir = std::filesystem::current_path();

std::string read_foregn_filename(pid_t pid, const char *filename) {
    std::string res;
    const size_t bufsz = 256;
    std::vector<char> buf(bufsz);
    std::vector<iovec> local_iovs(bufsz), remote_iovs(bufsz);
    for (size_t i = 0; i < bufsz; i++) {
        local_iovs[i].iov_base = buf.data() + i;
        local_iovs[i].iov_len = 1;
        remote_iovs[i].iov_base = const_cast<char *>(filename + i);
        remote_iovs[i].iov_len = 1;
    }
    size_t cur_pos = 0;
    while (true) {
        errno = 0;
        int iovs_read = process_vm_readv(pid, local_iovs.data(), bufsz, remote_iovs.data(), bufsz, 0);
        if (iovs_read < 0) {
            std::cout << "INVALID FILENAME " << strerror(errno) << ' ' << res << std::endl;
            exit(1);
        }
        bool str_finished = false;
        for (int i = 0; i < iovs_read; i++) {
            if (buf[i] == 0) {
                str_finished = true;
                break;
            }
            res.push_back(buf[i]);
        }
        if (str_finished) {
            break;
        }
        if (iovs_read < bufsz) {
            break;
        } else {
            cur_pos += bufsz;
            for (size_t i = 0; i < bufsz; i++) {
                remote_iovs[i].iov_base = const_cast<char *>(filename + i + cur_pos);
                remote_iovs[i].iov_len = 1;
            }
        }
    }
    return res;
}
void tracer(const Config &config) {
    int fork_limit_left = config.forks_limit;
    std::unordered_map<pid_t, PidStatus> tracees;
    std::map<int, int> syscall_blocked;
    auto blk_stat = [&](int i) {
        if (syscall_blocked.find(i) == syscall_blocked.end()) {
            syscall_blocked.insert(std::make_pair(i, 1));
        } else {
            syscall_blocked.at(i)++;
        }
    };
    bool executed = false;
    std::ofstream log;
    log.open(".sandbox.log", std::ios::out | std::ios::app);
    if (!log.is_open()) {
        std::cout << "unable to open log file" << std::endl;
        return; // TODO should we do smthg else?
    }
    log << "\n\n---- new tracer ----\nDATE\t\tPID\t\tEVENT" << std::endl;
    std::map<std::pair<pid_t, int>, std::string> fd_names;
    while (true) {
        int status;
        pid_t pid = waitpid(-1, &status, 0);
        if (pid < 0) {
            LOG("SBOX", "Get error " << errno << " from waitpid.");
            break;
        }

        if (tracees.count(pid) == 0) {
            tracees.emplace(pid, PidStatus{pid});
            tracees.at(pid).Resume();
            LOG(pid, "Born.");
            continue;
        }

        if (should_die) {
            LOG("SBOX", "Catched signal " << killed_signal << ". Stopping work.");
            std::cout << "Sandbox stopping...Please wait." << std::endl;
            for (auto i : tracees) {
                kill(i.first, SIGKILL);
            }
            LOG("SBOX", "Sent SIGKILL to all parts of suspect");
            should_die = 0;
            //continue;
        }

        if (WIFEXITED(status)) {
            tracees.erase(pid);
            LOG(pid, "Ended correctly.");
            if (tracees.empty()) {
                LOG("SBOX", "Ended correctly");
                std::cout << "Process ended" << std::endl;
                break;
            }
            continue;
        }

        if (WIFSIGNALED(status)) {
            tracees.erase(pid);
            LOG(pid, "Died from " << WTERMSIG(status) << " signal.");
            if (tracees.empty()) {
                LOG("SBOX", "Ended correctly");
                std::cout << "Process ended" << std::endl;
                break;
            }
            continue;
        }

        {
            auto &pid_state = tracees.at(pid);
            auto regs = pid_state.HandleStatus(status);
            if (regs && pid_state.current_state == PidStatus::kInSysCall) {
                auto state = *regs;
                switch (pid_state.lastsysno) {
                case SYS_clone:
                    if (fork_limit_left == 0) {
                        state.orig_rax = -1;
                        pid_state.SetRegisters(state);
                        LOG(pid, "Exceeded fork limit[REJECTED]");
                        blk_stat(SYS_clone);
                    } else {
                        fork_limit_left--;
                        LOG(pid, "Forked trough clone");
                    }
                    break;
                case SYS_fork:
                    if (fork_limit_left == 0) {
                        state.orig_rax = -1;
                        pid_state.SetRegisters(state);
                        LOG(pid, "Exceeded fork limit[REJECTED]");
                        blk_stat(SYS_fork);
                    } else {
                        fork_limit_left--;
                        LOG(pid, "Forked trough fork");
                    }
                    break;
                case SYS_vfork:
                    if (fork_limit_left == 0) {
                        state.orig_rax = -1;
                        pid_state.SetRegisters(state);
                        LOG(pid, "Exceeded fork limit[REJECTED]");
                        blk_stat(SYS_vfork);
                    } else {
                        fork_limit_left--;
                        LOG(pid, "Forked trough vfork");
                    }
                    break;
                case SYS_execve:
                    if (!executed) {
                        executed = true;
                        LOG(pid, "Used ececve");
                    } else {
                        state.orig_rax = -1;
                        pid_state.SetRegisters(state);
                        LOG(pid, "Used exexcve again[REJECTED]");
                        blk_stat(SYS_execve);
                    }
                    break;
                case SYS_kill:
                    if (tracees.find((pid_t)state.rdi) == tracees.end()) {
                        state.orig_rax = -1;
                        pid_state.SetRegisters(state);
                        LOG(pid, "Sent signal " << (int)state.rsi << " outside the programm[REJECTED]");
                        blk_stat(SYS_kill);
                    } else {
                        LOG(pid, "Sent signal " << (int)state.rsi << " to " << (pid_t)state.rdi);
                    }
                    break;
                case SYS_rt_sigaction:
                    if ((int)state.rdi == SIGXCPU) {
                        state.orig_rax = -1;
                        pid_state.SetRegisters(state);
                        LOG(pid, "Handled SIGXCPU signal[REJECTED]");
                        blk_stat(SYS_rt_sigaction);
                    }
                    break;
                case SYS_open: {
                    int flags = (int)state.rsi;
                    std::string filename = read_foregn_filename(pid, reinterpret_cast<const char *>(state.rdi));
                    auto abspath = curdir;
                    abspath.append(filename);
                    auto path_str = abspath.lexically_normal().string();
                    if (config.write_allowed.find(path_str) == config.write_allowed.end()) {
                        if ((flags & O_WRONLY) || (flags & O_RDWR)) {
                            state.orig_rax = -1;
                            pid_state.SetRegisters(state);
                            LOG(pid, "Write to not allowed file[REJECTED]");
                            blk_stat(SYS_open);
                        }
                    } else if (config.read_banned.find(path_str) == config.read_banned.end()) {
                        if (flags & O_RDONLY) {
                            state.orig_rax = -1;
                            pid_state.SetRegisters(state);
                            LOG(pid, "Read from not allowed file[REJECTED]");
                            blk_stat(SYS_open);
                        }
                    }
                    break;
                }
                case SYS_openat: {
                    int dfd = (int)state.rdi;
                    int flags = (int)state.rdx;
                    std::string filename = read_foregn_filename(pid, reinterpret_cast<const char *>(state.rsi));
                    auto abspath = curdir;
                    if (!filename.empty() && filename[0] == '/') {
                        abspath = std::filesystem::path(filename);
                    } else {
                        if (dfd == AT_FDCWD) {
                            abspath.append(filename);
                        } else {
                            abspath = std::filesystem::path(fd_names[{pid, dfd}]);
                        }
                    }
                    auto path_str = abspath.lexically_normal().string();
                    if (config.write_allowed.find(path_str) == config.write_allowed.end()) {
                        if ((flags & O_WRONLY) || (flags & O_RDWR)) {
                            state.orig_rax = -1;
                            pid_state.SetRegisters(state);
                            LOG(pid, "Write to not allowed file[REJECTED]");
                            blk_stat(SYS_openat);
                        }
                    } else if (config.read_banned.find(path_str) != config.read_banned.end()) {
                        if (flags & O_RDONLY) {
                            state.orig_rax = -1;
                            pid_state.SetRegisters(state);
                            LOG(pid, "Read from not allowed file[REJECTED]");
                            blk_stat(SYS_openat);
                        }
                    }
                    break;
                }
                default: {
                    int call_num = (int)pid_state.lastsysno;
                    if (config.traced_syscalls.find(call_num) != config.traced_syscalls.end()) {
                        char *syscall_name = seccomp_syscall_resolve_num_arch(SCMP_ARCH_X86_64, call_num);
                        state.orig_rax = -1;
                        pid_state.SetRegisters(state);
                        LOG(pid, "System call [" << syscall_name << "] [REJECTED]");
                        free(syscall_name);
                    }
                    break;
                }
                }
            } else if (regs && pid_state.current_state == PidStatus::kBeforeSysCall) {
                auto state = *regs;
                if (state.orig_rax == (uint64_t)-1) {
                    state.rax = -EPERM;
                    pid_state.SetRegisters(state);
                } else {
                    switch (pid_state.lastsysno) {
                    case SYS_open: {
                        int flags = (int)state.rsi;
                        std::string filename = read_foregn_filename(pid, reinterpret_cast<const char *>(state.rdi));
                        auto abspath = curdir;
                        abspath.append(filename);
                        auto path_str = abspath.lexically_normal().string();
                        int fd = (int)state.rax;
                        fd_names[{pid, fd}] = path_str;
                        break;
                    }
                    case SYS_openat: {
                        int dfd = (int)state.rdi;
                        int flags = (int)state.rdx;
                        std::string filename = read_foregn_filename(pid, reinterpret_cast<const char *>(state.rsi));
                        auto abspath = curdir;
                        if (!filename.empty() && filename[0] == '/') {
                            abspath = std::filesystem::path(filename);
                        } else {
                            if (dfd == AT_FDCWD) {
                                abspath.append(filename);
                            } else {
                                abspath = std::filesystem::path(fd_names[{pid, dfd}]);
                            }
                        }
                        auto path_str = abspath.lexically_normal().string();
                        int fd = (int)state.rax;
                        fd_names[{pid, fd}] = path_str;
                        break;
                    }
                    default:
                        break;
                    }
                }
            }
        }
        tracees.at(pid).Resume();
    }
    log.close();
    std::cout << "Following system calls were blocked:" << std::endl;
    std::ofstream stat;
    stat.open(".sandbox.stat", std::ios::out /*| std::ios::app*/);
    if (!stat.is_open()) {
        std::cout << "unable to open stat file" << std::endl;
        return;
    }
    for (auto i : syscall_blocked) {
        char *syscall_name = seccomp_syscall_resolve_num_arch(SCMP_ARCH_X86_64, i.first);
        stat << syscall_name << ' ' << i.second << std::endl;
        std::cout << "\tSystem call [" << syscall_name << "] was blocked " << i.second << " times" << std::endl;
        free(syscall_name);
    }
    std::cout << "For more detailed information see .sandbox.log file." << std::endl;
    stat.close();
}

void signal_handler(int signal) {
    killed_signal = signal;
    should_die = true;
}

int main(int argc, char **argv) {
    std::string config_path = "./configuration.toml";
    int program_pos;
    if (argc < 3 || strcmp(argv[1], "--config-path") != 0) {
        config_path = "./configuration.toml";
        program_pos = 1;
    } else {
        config_path = std::string(argv[2]);
        program_pos = 3;
    }
    auto toml_config = toml::parse_file(config_path);

    auto process_limits = *toml_config["process"].as_table();
    rlim_t memory_limit = (rlim_t)process_limits["memory_limit"].value_or(RLIM_INFINITY);
    rlim_t time_limit = (rlim_t)process_limits["time_limit"].value_or(RLIM_INFINITY);
    int forks_limit = (int)process_limits["fork_limit"].value_or(0);
    int niceness = (int)process_limits["niceness"].value_or(0);

    auto filesystem_limits = *toml_config["filesystem"].as_table();
    auto array_parser = [&](const toml::table &tb, const std::string_view key) -> std::unordered_set<std::string> {
        std::unordered_set<std::string> res;
        if (!tb[key].is_array()) {
            return res;
        }
        auto arr = tb[key].as_array();
        for (auto &item : *arr) {
            auto s = item.as_string()->value_or(std::string(""));
            if (!s.empty()) {
                res.insert(s);
            }
        }
        return res;
    };
    auto make_paths_absolute = [&](std::unordered_set<std::string> paths) -> std::unordered_set<std::string> {
        std::unordered_set<std::string> res;
        for (auto path_string : paths) {
            if (path_string.empty())
                continue;
            if (path_string[0] == '/') {
                std::filesystem::path p(path_string);
                res.insert(p.lexically_normal().string());
            } else {
                auto p = curdir;
                res.insert(p.append(path_string).lexically_normal().string());
            }
        }
        return res;
    };
    auto read_banned = make_paths_absolute(array_parser(filesystem_limits, "read_banned"));
    auto write_allowed = make_paths_absolute(array_parser(filesystem_limits, "write_allowed"));

    auto syscalls = *toml_config["syscalls"].as_table();
    auto additional_traced_ban = array_parser(syscalls, "additional_traced_ban");
    std::unordered_set<int> banned_syscalls{
        SYS_ptrace,       SYS_mount,         SYS_umount2,       SYS_move_mount,   SYS_setfsgid, SYS_setuid,
        SYS_setgid,       SYS_setfsuid,      SYS_sethostname,   SYS_setgroups,    SYS_setregid, SYS_setsid,
        SYS_settimeofday, SYS_setdomainname, SYS_seccomp,       SYS_setns,        SYS_shutdown, SYS_reboot,
        SYS_socket,       SYS_socketpair,    SYS_chdir,         SYS_chmod,        SYS_chown,    SYS_chroot,
        SYS_fchdir,       SYS_fchmod,        SYS_fchmodat,      SYS_fchown,       SYS_fchownat, SYS_rename,
        SYS_renameat2,    SYS_renameat,      SYS_link,          SYS_linkat,       SYS_unlink,   SYS_unlinkat,
        SYS_umask,        SYS_capset,        SYS_mknod,         SYS_pivot_root,   SYS_bpf,      SYS_vhangup,
        SYS__sysctl,      SYS_swapoff,       SYS_swapon,        SYS_setrlimit,    SYS_acct,     SYS_iopl,
        SYS_ioperm,       SYS_init_module,   SYS_delete_module, SYS_query_module, SYS_quotactl, SYS_remap_file_pages,
        SYS_exit_group,   SYS_migrate_pages, SYS_unshare,       SYS_move_pages,   SYS_dup,      SYS_dup2,
        SYS_dup3,         SYS_finit_module,  SYS_setxattr};
    std::unordered_set<int> traced_syscalls{SYS_fork,   SYS_clone,  SYS_vfork, SYS_open,
                                            SYS_execve, SYS_openat, SYS_kill,  SYS_rt_sigaction};
    for (const auto &syscall : additional_traced_ban) {
        int syscall_number = (int)seccomp_syscall_resolve_name(syscall.c_str());
        if (syscall_number != 0) {
            if (banned_syscalls.find(syscall_number) != banned_syscalls.end()) {
                banned_syscalls.erase(syscall_number);
            }
            traced_syscalls.insert(syscall_number);
        }
    }
    Config config{forks_limit, read_banned, write_allowed, traced_syscalls};

    pid_t pid = fork();
    if (pid == -1) {
        std::cerr << "LOL" << std::endl;
        return 0;
    }
    if (pid != 0) {
        std::signal(SIGINT, signal_handler);
        std::signal(SIGHUP, signal_handler);
        std::signal(SIGXCPU, signal_handler);
        std::signal(SIGTERM, signal_handler);
        std::signal(SIGQUIT, signal_handler);
        tracer(config);
    } else {
        if (ptrace(PTRACE_TRACEME, 0, NULL, NULL) < 0) {
            die("child: ptrace(traceme) failed: %m");
        }
        /* Остановиться и дождаться, пока отладчик отреагирует. */
        if (raise(SIGSTOP)) {
            die("child: raise(SIGSTOP) failed: %m");
        }
        errno = 0;
        nice(niceness);
        if (errno != 0) {
            if (errno == EPERM) {
                std::cout << "You do not have permissions on decreasing niceness" << std::endl;
            } else {
                std::cout << "Failure " << errno << ' ' << strerror(errno) << std::endl;
            }
            exit(1);
        }
        errno = 0;
        rlimit mem_limit{memory_limit, memory_limit};
        rlimit cpu_limit{time_limit, time_limit};
        setrlimit(RLIMIT_AS, &mem_limit);
        if (errno != 0) {
            std::cout << "Fail to set memory limit" << std::endl;
            exit(1);
        }
        setrlimit(RLIMIT_CPU, &cpu_limit);
        if (errno != 0) {
            std::cout << "Fail to set CPU limit" << std::endl;
            exit(1);
        }
        scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_ALLOW);
        for (int syscall : banned_syscalls) {
            seccomp_rule_add(ctx, SCMP_ACT_ERRNO(EPERM), syscall, 0);
        }
        for (int syscall : traced_syscalls) {
            seccomp_rule_add(ctx, SCMP_ACT_TRACE(0), syscall, 0);
        }
        seccomp_load(ctx);
        std::vector<char *> buf_args;
        for (int i = program_pos; i < argc; i++) {
            buf_args.push_back(argv[i]);
        }
        buf_args.push_back(nullptr);
        errno = 0;
        if (execvp(buf_args[0], buf_args.data()) < 0) {
            if (errno == EACCES) {
                std::cout << "File not executable" << std::endl;
            } else {
                std::cout << "Could not run " << buf_args[0] << std::endl;
            }
            return 0;
        }
    }
}
