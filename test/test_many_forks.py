import subprocess
import time


def test_forks():
    proc = subprocess.Popen(["./sandbox",
        "build/test/many-forks.elf"])
    time.sleep(1.5)
    if proc.poll == None:
        proc.terminate()
        time.sleep(0.5)
    with open(".sandbox.stat", "r") as f:
        line = f.readline().strip()
        assert line == "clone 1"
