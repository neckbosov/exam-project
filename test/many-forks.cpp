#include <iostream>
#include <unistd.h>

void tracee(int forks_remaining) {
    if (forks_remaining == 0) {
        sleep(10);
        return;
    }
    pid_t pid = fork();
    if (pid == -1) {
        return;
    }
    if (pid != 0) {
        sleep(1);
    } else {
        tracee(forks_remaining - 1);
    }
}

int main() {
    int forks_remaining = 6;
    tracee(forks_remaining);
    return 0;
}