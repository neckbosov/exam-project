#include <fcntl.h>
#include <unistd.h>
#include <iostream>
int main() {
    int fd = open("/dev/null", O_WRONLY);
    std::cout << fd << std::endl;
    close(fd);
    return 0;
}