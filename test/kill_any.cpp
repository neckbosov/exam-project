#include <unistd.h>
#include <csignal>

int main() {
    kill(2, SIGKILL);
    return 0;
}