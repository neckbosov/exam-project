import subprocess
import time


def test_open():
    proc = subprocess.Popen(["./sandbox",
        "build/test/open-banned.elf"])
    time.sleep(0.5)
    if proc.poll == None:
        proc.terminate()
        time.sleep(0.5)
    with open(".sandbox.stat", "r") as f:
        line = f.readline().strip()
        assert line == "openat 1"
