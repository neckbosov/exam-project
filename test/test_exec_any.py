import subprocess
import time


def test_exec():
    proc = subprocess.Popen(["./sandbox",
        "build/test/exec_any.elf"])
    time.sleep(0.2)
    if proc.poll == None:
        proc.terminate()
        time.sleep(0.2)
    with open(".sandbox.stat", "r") as f:
        line = f.readline().strip()
        assert line == "execve 1"
