CPP = g++
CFLAGS = -O2 -Wshadow -Wall -Wextra -std=c++17
EXE = sandbox
EXE_OBJECTS = build/main/main.o
DYNAMIC_LIBS = -lseccomp -lpthread
TEST_OBJECTS = build/test/many-forks.elf build/test/open-banned.elf build/test/kill_any.elf build/test/exec_any.elf

all : build $(EXE)
test : build $(TEST_OBJECTS) $(EXE)
	PYTHONDONTWRITEBYTECODE=1 pytest -p no:cacheprovider 

build:
	mkdir -p build 
	mkdir -p build/main
	mkdir -p build/test

$(EXE) : $(EXE_OBJECTS)
	$(CPP) $(EXE_OBJECTS) -o $(EXE) $(DYNAMIC_LIBS)


build/main/%.o : src/%.cpp
	$(CPP) $(CFLAGS) -I./src -c -o $@ $<

build/test/%.elf : test/%.cpp
	$(CPP) $(CFLAGS) -I./test -o $@ $<

clean:
	rm build/main/*.o build/test/*.elf $(EXE)

.PHONY: all clean test

build/main/main.o: src/toml.hpp
